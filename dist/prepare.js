"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Mongo = __importStar(require("mongodb"));
const names = [
    "Alpha",
    "Beta",
    "Gama",
    "Delta",
    "Epsilon",
    "Zeta",
    "Eta",
    "Theta",
    "Iota",
    "Kappa",
    "Mu",
    "Nu",
    "Xi",
    "Omicron",
    "Pi",
    "Rho",
    "Sigma",
    "Tau",
    "Upsilon",
    "Phi",
    "Chi",
    "Psi",
    "Omega",
];
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
async function main() {
    const dbName = "Fittest";
    const dbUri = "mongodb://localhost:27017";
    const client = new Mongo.MongoClient(dbUri);
    await client.connect();
    const db = client.db(dbName);
    await db.dropCollection("Subjects");
    const collection = await db.createCollection("Subjects");
    const subjects = [];
    for (let i = 0; i < 100; i++) {
        const values = [];
        for (let j = 0; j < 10; j++) {
            values.push(getRandomInt(10));
        }
        subjects.push({
            name: names[getRandomInt(names.length - 1)] +
                " " +
                names[getRandomInt(names.length - 1)] +
                " " +
                names[getRandomInt(names.length - 1)],
            values: values,
        });
    }
    console.log(subjects);
    await collection.insertMany(subjects);
    await client.close();
}
main()
    .then(() => {
    console.log("Done");
})
    .catch((err) => console.error(err));
