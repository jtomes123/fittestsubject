"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Mongo = __importStar(require("mongodb"));
function calculateScore(arr1, arr2) {
    if (arr1.length !== arr2.length)
        throw new Error("Arrays must be of same length.");
    let sum = 0;
    for (let i = 0; i < arr1.length; i++) {
        sum += arr1[i] * arr2[i];
    }
    return sum;
}
function logStats(array, max, title) {
    let text = "\n-----" + title + "----\n";
    array.forEach((n, index) => {
        text += "\n" + (index + 1) + ".\t[";
        const squares = Math.floor((n / max) * 10);
        for (let i = 0; i < 10; i++) {
            text += squares > i ? "■" : "□";
        }
        text += "]";
    });
    console.log(text);
}
async function main() {
    const dbName = "Fittest";
    const dbUri = "mongodb://localhost:27017";
    const client = new Mongo.MongoClient(dbUri);
    await client.connect();
    const db = client.db(dbName);
    const collection = await db.collection("Subjects");
    const evaluationVector = [1, 5, 9, 1, 1, 4, 7, 1, 3, 2];
    logStats(evaluationVector, 9, "Selection Vector");
    const queryResult = await collection
        .find()
        .map((subject) => {
        return {
            name: subject.name,
            values: subject.values,
            score: calculateScore(subject.values, evaluationVector),
        };
    })
        .toArray();
    queryResult
        .sort((a, b) => b.score - a.score)
        .slice(0, 10)
        .forEach((e) => logStats(e.values, 9, e.name + " (" + e.score + ")"));
    client.close();
}
main()
    .then(() => {
    console.log("Done");
})
    .catch((err) => console.error(err));
