import * as Mongo from "mongodb";
import { ISubject } from "./ISubject";

function calculateScore(arr1: number[], arr2: number[]): number {
  if (arr1.length !== arr2.length)
    throw new Error("Arrays must be of same length.");

  let sum = 0;
  for (let i = 0; i < arr1.length; i++) {
    sum += arr1[i] * arr2[i];
  }

  return sum;
}

function logStats(array: number[], max: number, title: string) {
  let text = "\n-----" + title + "----\n";

  array.forEach((n, index) => {
    text += "\n" + (index + 1) + ".\t[";
    const squares = Math.floor((n / max) * 10);
    for (let i = 0; i < 10; i++) {
      text += squares > i ? "■" : "□";
    }
    text += "]";
  });

  console.log(text);
}

async function main() {
  const dbName = "Fittest";
  const dbUri = "mongodb://localhost:27017";

  const client = new Mongo.MongoClient(dbUri);

  await client.connect();

  const db = client.db(dbName);

  const collection = await db.collection<ISubject>("Subjects");

  const evaluationVector = [1, 5, 9, 1, 1, 4, 7, 1, 3, 2];

  logStats(evaluationVector, 9, "Selection Vector");

  const queryResult = await collection
    .find()
    .map((subject) => {
      return {
        name: subject.name,
        values: subject.values,
        score: calculateScore(subject.values, evaluationVector) as number,
      };
    })
    .toArray();
  queryResult
    .sort((a, b) => b.score - a.score)
    .slice(0, 10)
    .forEach((e) => logStats(e.values, 9, e.name + " (" + e.score + ")"));

  client.close();
}

main()
  .then(() => {
    console.log("Done");
  })
  .catch((err) => console.error(err));
