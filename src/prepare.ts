import * as Mongo from "mongodb";
import { ISubject } from "./ISubject";

const names = [
  "Alpha",
  "Beta",
  "Gama",
  "Delta",
  "Epsilon",
  "Zeta",
  "Eta",
  "Theta",
  "Iota",
  "Kappa",
  "Mu",
  "Nu",
  "Xi",
  "Omicron",
  "Pi",
  "Rho",
  "Sigma",
  "Tau",
  "Upsilon",
  "Phi",
  "Chi",
  "Psi",
  "Omega",
];

function getRandomInt(max: number): number {
  return Math.floor(Math.random() * Math.floor(max));
}

async function main() {
  const dbName = "Fittest";
  const dbUri = "mongodb://localhost:27017";

  const client = new Mongo.MongoClient(dbUri);

  await client.connect();

  const db = client.db(dbName);
  await db.dropCollection("Subjects");

  const collection = await db.createCollection("Subjects");
  const subjects: ISubject[] = [];

  for (let i = 0; i < 100; i++) {
    const values: number[] = [];
    for (let j = 0; j < 10; j++) {
      values.push(getRandomInt(10));
    }
    subjects.push({
      name:
        names[getRandomInt(names.length - 1)] +
        " " +
        names[getRandomInt(names.length - 1)] +
        " " +
        names[getRandomInt(names.length - 1)],
      values: values,
    });
  }

  console.log(subjects);

  await collection.insertMany(subjects);

  await client.close();
}

main()
  .then(() => {
    console.log("Done");
  })
  .catch((err) => console.error(err));
