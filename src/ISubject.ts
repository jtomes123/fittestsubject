export interface ISubject {
  name: string;
  values: number[];
}
